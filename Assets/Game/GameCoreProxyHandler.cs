﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class Game : MonoBehaviour
{
    //--------------------------------------------
    bool IsPaused = false;
    //--------------------------------------------
    public void OnLevelStarted(int mode)
    {
        #if UNITY_EDITOR
        Debug.Log("Game OnLevelStarted()");
        #endif
        HideBottlePopUp();
        EndGame();
        CreateLevel(mode);
        //GameplayUI.SetActive (true);
    }
    //--------------------------------------------
    public void OnLevelRevived(int mode)
    {
        #if UNITY_EDITOR
        Debug.Log("Game OnLevelRevived()");
        #endif
        Revive();
        //do all needed effects and continue game
    }
    //--------------------------------------------
    public void OnLevelEnded()
    {
        #if UNITY_EDITOR
        Debug.Log("Game OnLevelEnded()");
        #endif
        EndGame();

        GameplayUI.SetActive(false);
    }
    //--------------------------------------------
    public void OnLevelPaused()
    {
        #if UNITY_EDITOR
        Debug.Log("Game OnLevelPaused()");
        #endif
        paused = true;
        IsPaused = true;
    }
    //--------------------------------------------
    public void OnLevelResumed()
    {
        #if UNITY_EDITOR
        Debug.Log("Game OnLevelResumed()");
        #endif
        paused = false;
        IsPaused = false;
    }
    //--------------------------------------------
    public void OnReviveProposed()
    {
        #if UNITY_EDITOR
        Debug.Log("Game OnReviveProposed()");
        #endif
//        EndGame();
    }
    //--------------------------------------------
    public string OnAppSave()
    {
        #if UNITY_EDITOR
        Debug.Log("Game OnAppSave()");
        #endif

        return "";
    }
    //--------------------------------------------
    public void OnAppRestore(string state)
    {
        #if UNITY_EDITOR
        Debug.Log("Game OnAppRestore()");
        #endif
    }
    //--------------------------------------------
}
