﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class Game : ICoreHandler
{

    public GameObject GameplayUI;
    //--------------------------------------------
    void Start()
    {
        animPos = manAinm.transform.position;
        cameraDefault = Camera.main.transform.position;
        GameProxy.Instance.RegisterCoreHandler(this);
        GameplayUI.SetActive(false);
        InitSkins();
    }

    //--------------------------------------------
    public void UIAddScore()
    {
        GameProxy.Instance.AddScoreRotated(100, 0.5f, 0);
    }
    //--------------------------------------------
    public void UIAddCoins()
    {
        GameProxy.Instance.AddCoinsRotated(20, 5f, 0);
    }
    //--------------------------------------------
    public void UIDie()
    {
        GameProxy.Instance.EndLevel();
    }
    //--------------------------------------------
}
