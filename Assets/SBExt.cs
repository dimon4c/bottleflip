﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SBExt : MonoBehaviour
{
    public Scrollbar sb;
    public GameObject cont;

    void OnEnable()
    {
        sb.size = 0f;
        cont.transform.position -= new Vector3(0f, 1f);
    }
}
