﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class GameProxy : MonoBehaviour
{
    //--------------------------------------------
    public static GameProxy Instance;
    //--------------------------------------------
    ICoreHandler CoreHandler;
    //--------------------------------------------
    public int DoubleCoins{ get { return  PlayerPrefs.GetInt("DoubleCoins", 0); } set { PlayerPrefs.SetInt("DoubleCoins", value); } }

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            BestScores = new int[GameModeCount];
            RateAfterMinutesFirst *= 60;
            RateAfterMinutesNext *= 60;
        }
    }
    //--------------------------------------------
    void Start()
    {
		
        if (ResetGame)
        {
            ResetValues();
        }

        LoadPreferences();

        if (GamesPlayed == 0)
        {
            SetProtectedCoins(StartCoins);
        }

        InitVideo();

        HideGameUI();
        HideSecondUI();
        ShowHomeUI();

        ToggleSoundButtons();

        AdsUI.SetActive(FakeAds);

        if (BannerEnabled)
        {
            InitBanner();
        }
//        InitInterstitial();

        InitPlayServices();

        TestUI.SetActive(TestUIEnabled);
    }
    //--------------------------------------------
    public void RegisterCoreHandler(ICoreHandler handler)
    {
        CoreHandler = handler;
    }
    //--------------------------------------------
    void Update()
    {
        if (CanUserMove())
        {
            GameUIOnUpdate();
        }
        if (SkinsUI.activeSelf)
        {
            SkinsUIOnUpdate();
        }
    }
    //--------------------------------------------

}
