﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public partial class GameProxy
{

    string salt1 = "2ZJY5oT1";
    string salt2 = "BpzcJIxY";

    //--------------------------------------------
    // Protecting:
    // Coins
    // CoinsX
    // AdsRemoved
    //--------------------------------------------

    string CalcHash(string val)
    {
        string input = salt2 + "-" + val + "-" + salt1;
        string result = "";

        try
        {
            System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
            byte[] bytes = encoding.GetBytes(input);

            var sha = new System.Security.Cryptography.SHA1CryptoServiceProvider();
            result = System.BitConverter.ToString(sha.ComputeHash(bytes));
        }
        catch (Exception ex)
        {
            Debug.Log("Error in GetHash(): " + ex.ToString());
        }

        return result.Replace("-", "");
    }

    //--------------------------------------------
    void LoadPreferences()
    {
        LoadProtectedValues();
        LoadUnprotectedValues();
    }
    //--------------------------------------------
    bool TestInt(int val, string check)
    {
        if (string.IsNullOrEmpty(check))
            return false;
        string hash = CalcHash(val.ToString());
        return hash.Equals(check);
    }
    //--------------------------------------------
    void SetProtectedCoins(int val)
    {
        Coins = val;
        PlayerPrefs.SetInt("coins", Coins);
        PlayerPrefs.SetString("coins_ck", CalcHash(Coins.ToString()));
    }
    //--------------------------------------------
    void LoadProtectedValues()
    {
        bool passed = true;

        Coins = PlayerPrefs.GetInt("coins", 0);
        if (!TestInt(Coins, PlayerPrefs.GetString("coins_ck", "")))
        {
            passed = false;
        }

        CoinsXPerm = PlayerPrefs.GetInt("coins_x", 1);
        if (!TestInt(CoinsXPerm, PlayerPrefs.GetString("coins_x_ck", "")))
        {
            passed = false;
        }
			
        AdsRemoved = PlayerPrefs.GetInt("ads_removed", 0);
        if (!TestInt(AdsRemoved, PlayerPrefs.GetString("ads_removed_ck", "")))
        {
            passed = false;
        }

        for (int i = 0; i < GameModeCount; i++)
        {
            BestScores[i] = PlayerPrefs.GetInt("score" + i, 0);
            if (!TestInt(BestScores[i], PlayerPrefs.GetString("score" + i + "_ck", "")))
            {
                passed = false;
            }
        }

        if (!passed)
        {
            ResetValues();
        }

    }
    //--------------------------------------------
    void LoadUnprotectedValues()
    {
        SkinId = PlayerPrefs.GetInt("skin", 0);
        IsMuted = PlayerPrefs.GetInt("muted", 0);
        GamesPlayed = PlayerPrefs.GetInt("games_count", 0);
    }
    //--------------------------------------------
    void SaveMuted()
    {
        PlayerPrefs.SetInt("muted", IsMuted);
    }
    //--------------------------------------------
    void SaveUnprotectedValues()
    {
        PlayerPrefs.SetInt("skin", SkinId);
        PlayerPrefs.SetInt("muted", IsMuted);
        PlayerPrefs.SetInt("games_count", GamesPlayed);

        PlayerPrefs.Save();
    }
    //--------------------------------------------
    void SaveProtectedValues()
    {
        PlayerPrefs.SetInt("coins", Coins);
        PlayerPrefs.SetString("coins_ck", CalcHash(Coins.ToString()));

        PlayerPrefs.SetInt("coins_x", CoinsXPerm);
        string check = CalcHash(CoinsXPerm.ToString());
        PlayerPrefs.SetString("coins_x_ck", check);

        PlayerPrefs.SetInt("ads_removed", AdsRemoved);
        PlayerPrefs.SetString("ads_removed_ck", CalcHash(AdsRemoved.ToString()));

        for (int i = 0; i < GameModeCount; i++)
        {
            PlayerPrefs.SetInt("score" + i, BestScores[i]);
            PlayerPrefs.SetString("score" + i + "_ck", CalcHash(BestScores[i].ToString()));
        }

        PlayerPrefs.Save();
    }
    //--------------------------------------------
    void SaveAllPreferences()
    {
        SaveUnprotectedValues();
        SaveProtectedValues();
        PlayerPrefs.Save();
    }
    //--------------------------------------------
    void ResetValues()
    {
        PlayerPrefs.DeleteAll();
        Coins = 0;
        CoinsXPerm = 1;
        AdsRemoved = 0;
        Score = 0;
        IsMuted = 0;
        for (int i = 0; i < GameModeCount; i++)
        {
            BestScores[i] = 0;
        }
        SaveProtectedValues();
        Debug.Log("Preferences were reset!");
    }
    //--------------------------------------------
}
