﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GooglePlayGames;

public partial class GameProxy {
	//--------------------------------------------
	public string [] Leaderboards = new string[1];
	//--------------------------------------------
	bool IsLoggedIn = false;
	//--------------------------------------------
	void ReportBestScore()
	{
		if (!IsLoggedIn)
			return;
		
		string lbId = Leaderboards [GameMode];
		int score = BestScores [GameMode];
		Social.ReportScore(score, lbId, (bool success) =>
			{
				// handle success or failure
			});

	}
	//----------------------------------------------
	public void ReportAchievement(string name)
	{
		if (!IsLoggedIn)
			return;
		
		Social.ReportProgress(name, 100.0f, (bool success) =>
			{
				// handle success or failure
			});
	}
	//--------------------------------------------
	void InitPlayServices()
	{
		int lbOpenedPreviously = PlayerPrefs.GetInt ("lb_active",0);
		if (lbOpenedPreviously == 0) return; //only initialize if user has requested leaderboard manually at least once
			
		#if UNITY_ANDROID
		PlayGamesPlatform.Activate();
		PlayGamesPlatform.DebugLogEnabled = false;
		#endif

		Authenticate ();
	}
	//----------------------------------------------
	void Authenticate()
	{
		Social.localUser.Authenticate((bool success) =>
			{
				IsLoggedIn = success;
				if (IsLoggedIn) {
					FirstAuthCheck();
				}
			});
	}
	//----------------------------------------------
	void FirstAuthCheck()
	{
		int lbOpenedPreviously = PlayerPrefs.GetInt ("lb_active",0);
		if (lbOpenedPreviously == 0) {
			//send all currently achieved scores
			ReportAllBestScores();
			PlayerPrefs.SetInt ("lb_active", 1);
			PlayerPrefs.Save();
		}
	}
	//----------------------------------------------
	public void ShowLeaderboard()
	{
		if (IsLoggedIn)
		{
			#if UNITY_ANDROID
			((PlayGamesPlatform)Social.Active).ShowLeaderboardUI();
			#endif

			#if UNITY_IPHONE
			(Social.Active).ShowLeaderboardUI();
			#endif

		}
		else
		{
			Authenticate();
		}
	}
	//--------------------------------------------
	public void UIShowLeaderboard()
	{
		if (IsLoggedIn) {
			((PlayGamesPlatform)Social.Active).ShowLeaderboardUI();
		}

	}
	//--------------------------------------------
	void ReportAllBestScores()
	{
		//report all currently saved best scores
		for (int i=0; i<GameModeCount; i++) {
			string lbId = Leaderboards [GameMode];
			int score = BestScores [i];
			Social.ReportScore(score, lbId, (bool success) =>
				{
					// handle success or failure
				});
		}
	}
	//--------------------------------------------
}
