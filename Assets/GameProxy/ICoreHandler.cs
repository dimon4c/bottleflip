﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ICoreHandler
{
    void OnLevelStarted(int mode);

    void OnLevelRevived(int mode);

    void OnLevelEnded();

    void OnReviveProposed();

    void OnLevelPaused();

    void OnLevelResumed();

    string OnAppSave();

    void OnAppRestore(string json);

    void OnSkinScroll(int currentSkin);
}
