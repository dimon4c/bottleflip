﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public partial class GameProxy : MonoBehaviour
{
    //--------------------------------------------
    public bool TestUIEnabled = false;
    public bool ResetGame = false;
    public bool KeepHomeAfterStart = false;
    public bool KeepSecondAfterStart = false;
    public bool KeepGameAfterEnd = false;
    public bool SkipSecondUIForward = false;
    public bool SkipSecondUIBackward = false;
    public bool CoinsXModeEnabled = true;
    public bool FakeAds = false;
    public bool FakeStore = false;
    public bool FakeLeaderboard = false;
    public int CoinsXMultiplier = 2;
    public bool BannerEnabled = true;
    public int GameModeCount = 1;
    public int StartCoins = 0;
    public int MaxRevivesCount = 1;
    public int ReviveCost = 100;
    public int RewardAmount = 100;

    public int RateAfterGamesCount = 1;
    public float RateAfterMinutesFirst = 1;
    public float RateAfterMinutesNext = 2;
    //--------------------------------------------
    public GameObject HomeUI;
    public GameObject SecondUI;
    public GameObject GameUI;
    public GameObject PauseUI;
    public GameObject GameOverUI;
    public GameObject StoreUI;
    public GameObject SkinsUI;
    public GameObject TestUI;
    public GameObject AdsUI;
    public GameObject RateUI;

    public GameObject FakeBanner;
    public GameObject FakeInterstitial;
    public GameObject FakeStaticInterstitial;
    public GameObject FakeVideo;

    public Text MessageTxt;

    //--------------------------------------------
    public GameObject StoreCoinsTxt;
    public GameObject StoreCoinsShadTxt;
    //--------------------------------------------
    public GameObject SkinsCoinsTxt;
    public GameObject SkinsCoinsShadTxt;

    //--------------------------------------------
    void UpdateAllUIObjects()
    {
        HomeUIUpdateText();
        SecondUIUpdate();
        GameUIUpdate();

        /*
		PauseUIUpdate ();
		GameOverUIUpdate ();
		StoreUIUpdate ();
		SkinsUIUpdate ();
		*/
    }


    //--------------------------------------------
    void ObjectShowMessage(string text)
    {
        MessageTxt.text = text;
        MessageTxt.gameObject.transform.parent.gameObject.SetActive(true);
        Invoke("ObjectHideMessage", 2f);
    }

    //--------------------------------------------
    void ObjectHideMessage()
    {
        MessageTxt.gameObject.transform.parent.gameObject.SetActive(false);
    }
    //--------------------------------------------



}
