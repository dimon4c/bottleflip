﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


enum RateStatus {
	NotAskedYet,
	Later,
	Never,
	Yes
}

public partial class GameProxy : MonoBehaviour {

	//--------------------------------------------
	public void RateUI_OnNoBtn()
	{
		PlayerPrefs.SetInt ("rate_status", (int)RateStatus.Never);
		PlayerPrefs.Save ();
		HideRateUI ();
	}
	//--------------------------------------------
	public void RateUI_OnLaterBtn()
	{
		PlayerPrefs.SetFloat ("rate_time", 0);
		PlayerPrefs.SetInt ("rate_status", (int)RateStatus.Later);
		PlayerPrefs.Save ();
		HideRateUI ();
	}
	//--------------------------------------------
	public void RateUI_OnYesBtn()
	{
		PlayerPrefs.SetInt ("rate_status", (int)RateStatus.Yes);
		PlayerPrefs.Save ();
		HideRateUI ();
	}
	//--------------------------------------------
	void ShowRateUI()
	{
		RateUI.SetActive (true);
	}
	//--------------------------------------------
	void HideRateUI()
	{
		RateUI.SetActive (false);
	}
	//--------------------------------------------
	bool CheckRate()
	{
		RateStatus status = (RateStatus) PlayerPrefs.GetInt ("rate_status", 0);
		float rateTime = PlayerPrefs.GetFloat ("rate_time", 0);
		rateTime += ActiveTime;

		if (status == RateStatus.NotAskedYet) {
			//never asked, never rated
			if (rateTime >= RateAfterMinutesFirst) {
				ShowRateUI ();
			}
		} else if (status == RateStatus.Later) {
			//already asked, response was Later, we will check next time
			if (rateTime >= RateAfterMinutesNext) {
				ShowRateUI ();
			}

		} else if (status == RateStatus.Never) {
			//already asked, response was Never
		} else if (status == RateStatus.Yes) {
			//already asked, and response was Yes, hopefully rated
		}

		PlayerPrefs.SetFloat ("rate_time", rateTime);
		PlayerPrefs.Save ();

		return RateUI.activeSelf;
	}
}
