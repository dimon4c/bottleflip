﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class GameProxy
{

    public GameObject GameOverVideoBtn;
    //--------------------------------------------
    void GameOverUIUpdateText()
    {
        if (!GameOverUI.activeSelf)
            return;
        //do what needed
    }
    //--------------------------------------------
    public void ShowGameOverUI()
    {
        GameOverUI.SetActive(true);
    }
    //--------------------------------------------
    public void HideGameOverUI()
    {
        GameOverUI.SetActive(false);
    }
    //--------------------------------------------
    public void GameOverUI_OnHomeBtn()
    {
        CoreHandler.OnLevelEnded();

        HideGameOverUI();
        HideGameUI();

        ShowHomeUI();
        SaveAllPreferences();

        ShowInterstitial();
    }
    //--------------------------------------------
    public void GameOverUI_OnRestartBtn()
    {
        CoreHandler.OnLevelStarted(GameMode);
        ResetScore();
        HideGameOverUI();
        IsRunning = true;

    }
    //--------------------------------------------
    public void GameOverUI_OnReviveCoinsBtn()
    {
        if (SpendCoins(ReviveCost))
        {
            HideGameOverUI();
            ReviveGame();
        }
        else
        {
            ShowMessage("Not enough coins");
        }
    }
    //--------------------------------------------
    public void GameOverUI_OnReviveVideoBtn()
    {
        if (!ShowVideo(VideoRequestor.GameOverUI))
        {
            ShowMessage("Video is not available. Please try later");
        }
    }

    //--------------------------------------------
    void OnGameOverUIVideoLoaded()
    {
        //nothing to do
    }

    //--------------------------------------------
    void OnGameOverUIVideoWatched()
    {
        HideGameOverUI();
        ReviveGame();
    }
    //--------------------------------------------

}
