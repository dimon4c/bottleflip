﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;
using System;

public partial class GameProxy: MonoBehaviour, IStoreListener
{
    //*/
    void InitIAP()
    {

    }

    private static IStoreController m_StoreController;
    // The Unity Purchasing system.
    private static IExtensionProvider m_StoreExtensionProvider;
    // The store-specific Purchasing subsystems.

    // Product identifiers for all products capable of being purchased:
    // "convenience" general identifiers for use with Purchasing, and their store-specific identifier
    // counterparts for use with and outside of Unity Purchasing. Define store-specific identifiers
    // also on each platform's publisher dashboard (iTunes Connect, Google Play Developer Console, etc.)

    // General product identifiers for the consumable, non-consumable, and subscription products.
    // Use these handles in the code to reference which product to purchase. Also use these values
    // when defining the Product Identifiers on the store. Except, for illustration purposes, the
    // kProductIDSubscription - it has custom Apple and Google identifiers. We declare their store-
    // specific mapping to Unity Purchasing's AddProduct, below.

    public static string kProductIDConsumable1 = "com.bottle.flip.consumable.1";
    public static string kProductIDConsumable2 = "com.bottle.flip.consumable.2";
    public static string kProductIDConsumable3 = "com.bottle.flip.consumable.3";
    public static string kProductIDConsumable4 = "com.bottle.flip.consumable.4";
    public static string kProductIDConsumable5 = "com.bottle.flip.consumable.5";

    public static string kProductIDRemoveAds = "com.bottle.flip.remove_ads";
    public static string kProductIDDoubleCoins = "com.bottle.flip.doublecoins";

    //public static string kProductIDSubscription = "subscription";

    // Apple App Store-specific product identifier for the subscription product.
    //private static string kProductNameAppleSubscription = "com.unity3d.subscription.new";

    // Google Play Store-specific product identifier subscription product.
    //private static string kProductNameGooglePlaySubscription = "com.unity3d.subscription.original";

    public static bool IsInitializedOK;



    public void InitializePurchasing()
    {
        // If we have already connected to Purchasing ...
        if (IsInitialized())
        {
            // ... we are done here.
            return;
        }

        // Create a builder, first passing in a suite of Unity provided stores.
        var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());

        // Add a product to sell / restore by way of its identifier, associating the general identifier
        // with its store-specific identifiers.
        //builder.AddProduct(kProductIDConsumable1, ProductType.Consumable);

        builder.AddProduct(kProductIDConsumable1, ProductType.Consumable);
        builder.AddProduct(kProductIDConsumable2, ProductType.Consumable);
        builder.AddProduct(kProductIDConsumable3, ProductType.Consumable);
        builder.AddProduct(kProductIDConsumable4, ProductType.Consumable);
        builder.AddProduct(kProductIDConsumable5, ProductType.Consumable);

        // Continue adding the non-consumable product.
        builder.AddProduct(kProductIDRemoveAds, ProductType.NonConsumable);
        builder.AddProduct(kProductIDDoubleCoins, ProductType.NonConsumable);

        // And finish adding the subscription product. Notice this uses store-specific IDs, illustrating
        // if the Product ID was configured differently between Apple and Google stores. Also note that
        // one uses the general kProductIDSubscription handle inside the game - the store-specific IDs 
        // must only be referenced here. 
        /*
        builder.AddProduct(kProductIDSubscription, ProductType.Subscription, new IDs(){
            { kProductNameAppleSubscription, AppleAppStore.Name },
            { kProductNameGooglePlaySubscription, GooglePlay.Name },
        });*/

        // Kick off the remainder of the set-up with an asynchrounous call, passing the configuration 
        // and this class' instance. Expect a response either in OnInitialized or OnInitializeFailed.
        UnityPurchasing.Initialize(this, builder);
    }


    private bool IsInitialized()
    {
        // Only say we are initialized if both the Purchasing references are set.
        return m_StoreController != null && m_StoreExtensionProvider != null;
    }

    public void BuyConsumable(int id)
    {
        if (!IsInitializedOK)
            return;

        switch (id)
        {
            case 1: 
                BuyProductID(kProductIDConsumable1);
                break;
            case 2: 
                BuyProductID(kProductIDConsumable2);
                break;
            case 3: 
                BuyProductID(kProductIDConsumable3);
                break;
            case 4: 
                BuyProductID(kProductIDConsumable4);
                break;
            case 5: 
                BuyProductID(kProductIDConsumable5);
                break;
        }
    }

    public void BuyRemoveAds()
    {
        if (!IsInitializedOK)
            return;

        BuyProductID(kProductIDRemoveAds);
    }

    public void BuyDoubleCoins()
    {
        if (!IsInitializedOK)
            return;

        BuyProductID(kProductIDDoubleCoins);
    }

    /*
        public void BuySubscription()
        {
            // Buy the subscription product using its the general identifier. Expect a response either 
            // through ProcessPurchase or OnPurchaseFailed asynchronously.
            // Notice how we use the general product identifier in spite of this ID being mapped to
            // custom store-specific identifiers above.
            BuyProductID(kProductIDSubscription);
        }

    */
    void BuyProductID(string productId)
    {
        // If Purchasing has been initialized ...
        if (IsInitialized())
        {
            // ... look up the Product reference with the general product identifier and the Purchasing 
            // system's products collection.
            Product product = m_StoreController.products.WithID(productId);

            // If the look up found a product for this device's store and that product is ready to be sold ... 
            if (product != null && product.availableToPurchase)
            {
                Debug.Log(string.Format("Purchasing product asychronously: '{0}'", product.definition.id));
                // ... buy the product. Expect a response either through ProcessPurchase or OnPurchaseFailed 
                // asynchronously.
                m_StoreController.InitiatePurchase(product);
            }
			// Otherwise ...
			else
            {
                // ... report the product look-up failure situation  
                Debug.Log("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
            }
        }
		// Otherwise ...
		else
        {
            // ... report the fact Purchasing has not succeeded initializing yet. Consider waiting longer or 
            // retrying initiailization.
            Debug.Log("BuyProductID FAIL. Not initialized.");
            InitializePurchasing();
        }
    }


    // Restore purchases previously made by this customer. Some platforms automatically restore purchases, like Google.
    // Apple currently requires explicit purchase restoration for IAP, conditionally displaying a password prompt.
    public void RestorePurchases()
    {
        // If Purchasing has not yet been set up ...
        if (!IsInitialized())
        {
            // ... report the situation and stop restoring. Consider either waiting longer, or retrying initialization.
            Debug.Log("RestorePurchases FAIL. Not initialized.");
            return;
        }

        // If we are running on an Apple device ... 
        if (Application.platform == RuntimePlatform.IPhonePlayer ||
            Application.platform == RuntimePlatform.OSXPlayer)
        {
            // ... begin restoring purchases
            Debug.Log("RestorePurchases started ...");

            // Fetch the Apple store-specific subsystem.
            /*
            var apple = m_StoreExtensionProvider.GetExtension<IAppleExtensions>();
            // Begin the asynchronous process of restoring purchases. Expect a confirmation response in 
            // the Action<bool> below, and ProcessPurchase if there are previously purchased products to restore.
            apple.RestoreTransactions((result) => {
                // The first phase of restoration. If no more responses are received on ProcessPurchase then 
                // no purchases are available to be restored.
                Debug.Log("RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore.");
            });
            */
        }
		// Otherwise ...
		else
        {
            // We are not running on an Apple device. No work is necessary to restore purchases.
            Debug.Log("RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform);
        }
    }


    //
    // --- IStoreListener
    //
    public bool PricesLoaded;

    public string Price1 = "";
    public string Price2 = "";
    public string Price3 = "";
    public string Price4 = "";
    public string Price5 = "";
    public string Price6 = "";
    public string Price7 = "";

    public void LoadPrices()
    {
        string suffix = "";

        Price1 = 
			m_StoreController.products.WithID(kProductIDRemoveAds).metadata.localizedPriceString +
        suffix;

        Price2 = 
			m_StoreController.products.WithID(kProductIDDoubleCoins).metadata.localizedPriceString +
        suffix;

        Price3 = 
			m_StoreController.products.WithID(kProductIDConsumable1).metadata.localizedPriceString +
        suffix;

        Price4 = 
			m_StoreController.products.WithID(kProductIDConsumable2).metadata.localizedPriceString +
        suffix;

        Price5 = 
			m_StoreController.products.WithID(kProductIDConsumable3).metadata.localizedPriceString +
        suffix;

        Price6 = 
			m_StoreController.products.WithID(kProductIDConsumable4).metadata.localizedPriceString +
        suffix;

        Price7 = 
			m_StoreController.products.WithID(kProductIDConsumable5).metadata.localizedPriceString +
        suffix;


        PricesLoaded = true;
    }

    public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
    {
        // Purchasing has succeeded initializing. Collect our Purchasing references.
        Debug.Log("OnInitialized: PASS");

        // Overall Purchasing system, configured with products for this application.
        m_StoreController = controller;

        // Store specific subsystem, for accessing device-specific store features.
        m_StoreExtensionProvider = extensions;

        LoadPrices();

        IsInitializedOK = true;
    }


    public void OnInitializeFailed(InitializationFailureReason error)
    {
        // Purchasing set-up has not succeeded. Check error for reason. Consider sharing this reason with the user.
        Debug.Log("OnInitializeFailed InitializationFailureReason:" + error);

        IsInitializedOK = false;
    }


    public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
    {
        if (String.Equals(args.purchasedProduct.definition.id, kProductIDDoubleCoins, StringComparison.Ordinal))
        {
            Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
            DoubleCoinsPurchased();
        }
        else if (String.Equals(args.purchasedProduct.definition.id, kProductIDRemoveAds, StringComparison.Ordinal))
        {           
            OnAdsRemoved();
            Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
        }
        else if (String.Equals(args.purchasedProduct.definition.id, kProductIDConsumable1, StringComparison.Ordinal))
        {      
            CoinsPurchased(400);
            Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
        }
        else if (String.Equals(args.purchasedProduct.definition.id, kProductIDConsumable2, StringComparison.Ordinal))
        {      
            CoinsPurchased(1300);
            Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
        }
        else if (String.Equals(args.purchasedProduct.definition.id, kProductIDConsumable3, StringComparison.Ordinal))
        {      
            CoinsPurchased(2200);
            Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
        }
        else if (String.Equals(args.purchasedProduct.definition.id, kProductIDConsumable4, StringComparison.Ordinal))
        {      
            CoinsPurchased(5000);
            Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
        }
        else if (String.Equals(args.purchasedProduct.definition.id, kProductIDConsumable5, StringComparison.Ordinal))
        {      
            CoinsPurchased(10000);
            Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
        }

        // Return a flag indicating whether this product has completely been received, or if the application needs 
        // to be reminded of this purchase at next app launch. Use PurchaseProcessingResult.Pending when still 
        // saving purchased products to the cloud, and when that save is delayed. 
        return PurchaseProcessingResult.Complete;
    }
    //----------------------------------------------
    public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
    {
        // A product purchase attempt did not succeed. Check failureReason for more detail. Consider sharing 
        // this reason with the user to guide their troubleshooting actions.
        Debug.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));
    }
    //----------------------------------------------
    void OnAdsRemoved()
    {
        AdsRemoved = 1;
        SaveProtectedValues();
        /*/
		Game.Instance.UIShopPanelContent.transform.GetChild(0).gameObject.SetActive(false);
		Game.Instance.RemoveAdds = 1;
		Game.Instance.noAdds = true;
		Game.Instance.RemoveBanner();
		//*/
    }
    //----------------------------------------------
    void DoubleCoinsPurchased()
    {
        DoubleCoins = 1;
        /*
		Game.Instance.UIShopPanelContent.transform.GetChild(1).gameObject.SetActive(false);
		Game.Instance.OnDoubleCoinsPurchased();
		*/
    }
    //----------------------------------------------
    void CoinsPurchased(int count)
    {
        //*
        AddCoins(count);
        //*/
    }
    //----------------------------------------------
    //*/
}

