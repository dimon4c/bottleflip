﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class GameProxy {

	public GameObject SoundGameOn;
	public GameObject SoundGameOff;

	public GameObject SoundPauseOn;
	public GameObject SoundPauseOff;

	//--------------------------------------------
	void ToggleSoundButtons () {

		if (IsMuted == 0) {
			SoundGameOn.SetActive (true);
			SoundGameOff.SetActive (false);

			SoundPauseOn.SetActive (true);
			SoundPauseOff.SetActive (false);
		} else {
			SoundGameOn.SetActive (false);
			SoundGameOff.SetActive (true);

			SoundPauseOn.SetActive (false);
			SoundPauseOff.SetActive (true);
		}
	}
	//--------------------------------------------
	void Mute () {
		IsMuted = 1;
		ToggleSoundButtons ();
		SaveMuted ();
	}
	//--------------------------------------------
	void Unmute () {
		IsMuted = 0;
		ToggleSoundButtons ();
		SaveMuted ();
	}
	//--------------------------------------------
}
