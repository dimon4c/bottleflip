﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleMobileAds.Api;

public partial class GameProxy
{

    //--------------------------------------------
    public string AndroidBannerID;
    public string IOSBannerID;
    //--------------------------------------------
    BannerView banner;
    //--------------------------------------------
    void InitBanner()
    {
        FakeBanner.SetActive(false);

        if (AdsRemoved == 0)
        {

            #if UNITY_EDITOR
            if (FakeAds)
            {
                FakeBanner.SetActive(true);
                return;
            }
            #endif

            string bannerId = null;

            #if UNITY_ANDROID
            bannerId = AndroidBannerID;
            #endif

            #if UNITY_IOS
			bannerId = IOSBannerID;
            #endif

            banner = new BannerView(bannerId, AdSize.Banner, AdPosition.Top);
            AdRequest request = new AdRequest.Builder().Build();
            banner.LoadAd(request);
        }
    }
    //--------------------------------------------
    void HideBanner()
    {
        #if UNITY_EDITOR
        if (FakeAds)
        {
            FakeBanner.SetActive(false);
        }
        #endif

        if (banner != null)
        {
            banner.Hide();
            banner.Destroy();
        }
    }
    //--------------------------------------------
}
