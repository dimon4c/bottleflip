﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class GameProxy
{
    //--------------------------------------------
    void PauseUIUpdate()
    {

    }
    //--------------------------------------------
    void HidePauseUI()
    {
        PauseUI.SetActive(false);
    }
    //--------------------------------------------
    void ShowPauseUI()
    {
        PauseUI.SetActive(true);
        PauseUIUpdate();
    }
    //--------------------------------------------

   
    //--------------------------------------------
    // UI Event Handlers
    //--------------------------------------------
    public void PauseUI_OnResumeBtn()
    {
        HidePauseUI();
        ResumeGameUI();
    }
    //--------------------------------------------
    public void PauseUI_OnHomeBtn()
    {
        CoreHandler.OnLevelEnded();

        HidePauseUI();
        HideGameUI();

        ShowHomeUI();
        SaveAllPreferences();

        ShowInterstitial();

    }
    //--------------------------------------------
    public void PauseUI_OnSoundDisableBtn()
    {
        Mute();
    }
    //--------------------------------------------
    public void PauseUI_OnSoundEnableBtn()
    {
        Unmute();
    }
    //--------------------------------------------
    public void PauseUI_OnRestartBtn()
    {
        CoreHandler.OnLevelStarted(GameMode);
        ResetScore();
        HidePauseUI();
        IsRunning = true;

    }
}
