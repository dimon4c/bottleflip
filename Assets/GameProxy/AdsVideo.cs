﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleMobileAds.Api;
using System;
//--------------------------------------------
enum VideoRequestor {
	HomeUI,
	SecondUI,
	GameUI,
	GameOverUI
}
//--------------------------------------------
public partial class GameProxy {

	//--------------------------------------------
	public string AndroidVideoID;
	public string IOSVideoID;
	//--------------------------------------------
	VideoRequestor lastRequestor;
	//--------------------------------------------
	RewardBasedVideoAd video;
	//--------------------------------------------
	public void InitVideo()
	{ 
		FakeVideo.SetActive(false);
		#if UNITY_EDITOR
		if (FakeAds) {
			return;
		}
		#endif


		if (video == null) {
			video = RewardBasedVideoAd.Instance;
			video.OnAdRewarded += OnVideoWatched;
			video.OnAdLoaded += OnVideoLoaded;
		}
		LoadVideo ();
	}
	//--------------------------------------------
	public void LoadVideo()
	{        
		AdRequest request = new AdRequest.Builder().Build();

		#if UNITY_ANDROID
		video.LoadAd(request, AndroidVideoID);
		#endif

		#if UNITY_IOS
		video.LoadAd(request, IOSVideoID);
		#endif

	}
	//--------------------------------------------
	bool IsVideoLoaded()
	{
		#if UNITY_EDITOR
		if (FakeAds) {
			return true;
		}
		#endif

		return (video != null && video.IsLoaded ());
	}
	//--------------------------------------------
	bool ShowVideo(VideoRequestor requestor)
	{
		lastRequestor = requestor;

		#if UNITY_EDITOR
		if (FakeAds) {
			FakeVideo.SetActive(true);
			Invoke("DoOnVideoLoaded",5f);
			return true;
		}
		#endif

		if (video.IsLoaded ()) {
			video.Show ();
			return true;
		} else {
			LoadVideo ();
			return false;
		}
	}
	//--------------------------------------------
	void DoOnVideoLoaded()
	{
		OnHomeUIVideoLoaded ();
		OnSecondUIVideoLoaded ();
		OnGameUIVideoLoaded ();
		OnGameOverUIVideoLoaded ();
	}
	//--------------------------------------------
	void OnVideoLoaded(object sender, EventArgs args)
	{
		DoOnVideoLoaded ();
	}
	//--------------------------------------------
	void OnVideoWatched(object sender, Reward args)
	{
		if (lastRequestor == VideoRequestor.HomeUI) {
			OnHomeUIVideoWatched ();
		} else if (lastRequestor == VideoRequestor.SecondUI) {
			OnSecondUIVideoWatched ();
		} else if (lastRequestor == VideoRequestor.GameUI) {
			OnGameUIVideoWatched ();
		} else if (lastRequestor == VideoRequestor.GameOverUI) {
			OnGameOverUIVideoWatched ();
		} 
	}
	//--------------------------------------------
	public void AdsUI_FakeVideoWatched()
	{
		FakeVideo.SetActive (false);

		if (lastRequestor == VideoRequestor.HomeUI) {
			OnHomeUIVideoWatched ();
		} else if (lastRequestor == VideoRequestor.SecondUI) {
			OnSecondUIVideoWatched ();
		} else if (lastRequestor == VideoRequestor.GameUI) {
			OnGameUIVideoWatched ();
		} else if (lastRequestor == VideoRequestor.GameOverUI) {
			OnGameOverUIVideoWatched ();
		} 

	}
	//--------------------------------------------
}
