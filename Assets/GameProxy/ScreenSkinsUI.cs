﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class GameProxy
{


    public Transform SkinScrollPanel;
    GameObject[] skinsItems;
    public Transform SkinsCenter;
    bool isDragging;
    bool posChanged;
    int currentItem = 0;

    void ShowSkinsUI()
    {
        if (skinsItems == null)
        {
            InitLayout();
        }
        SkinsUI.SetActive(true);
    }

    void HideSkinsUI()
    {
        SkinsUI.SetActive(false);
    }

    void InitLayout()
    {
        skinsItems = new GameObject[SkinScrollPanel.childCount];
        for (int i = 0; i < skinsItems.Length; i++)
        {
            skinsItems[i] = SkinScrollPanel.transform.GetChild(i).gameObject;
        }
    }

    public void SkinsUILeft()
    {
        if (currentItem >= skinsItems.Length - 1)
            return;

        for (int i = 0; i < skinsItems.Length; i++)
        {
            skinsItems[i].transform.localScale = Vector2.one;
        }
        currentItem++;
        posChanged = true;
        CoreHandler.OnSkinScroll(currentItem);

        //float dist = skinsItems [currentItem + 1].transform.position.x - skinsItems [currentItem].transform.position.x;
        //SkinScrollPanel.transform.position = new Vector2 (SkinScrollPanel.transform.position.x - dist - 1f, SkinScrollPanel.transform.position.y);
        //SkinsUIStopDrag ();
    }

    public void SkinsUIRight()
    {
        if (currentItem <= 0)
            return;

        for (int i = 0; i < skinsItems.Length; i++)
        {
            skinsItems[i].transform.localScale = Vector2.one;
        }
        currentItem--;
        posChanged = true;
        CoreHandler.OnSkinScroll(currentItem);

        //float dist = skinsItems [currentItem].transform.position.x - skinsItems [currentItem - 1].transform.position.x;
        //SkinScrollPanel.transform.position = new Vector2 (SkinScrollPanel.transform.position.x + dist + 1f, SkinScrollPanel.transform.position.y);
        //SkinsUIStopDrag ();
    }

    void SkinsUIOnUpdate()
    {
        if (skinsItems == null)
            return;
		
        if (!isDragging && posChanged)
        {

            float delta = skinsItems[currentItem].transform.position.x - SkinsCenter.position.x;
            skinsItems[currentItem].transform.localScale = new Vector2(1.2f, 1.2f);
            Vector2 newPos = new Vector2(SkinScrollPanel.transform.position.x - delta, SkinScrollPanel.transform.position.y);
            if (Mathf.Abs(SkinScrollPanel.transform.position.x - newPos.x) > 0.03f)
            {
                SkinScrollPanel.transform.position = Vector2.Lerp(SkinScrollPanel.transform.position, newPos, Time.deltaTime * 20f);
            }
            else
            {
                posChanged = false;
            }
        }

    }

    public void SetCurrentSkin(int id)
    {
        currentItem = id;
        posChanged = true;
        //Debug.Log ("SkinsUIStartDrag");
    }

    public void SkinsUIStartDrag()
    {
        isDragging = true;
        //Debug.Log ("SkinsUIStartDrag");
    }

    public void SkinsUIStopDrag()
    {
        isDragging = false;
        float min = int.MaxValue;
        int minIndex = -1;

        for (int i = 0; i < skinsItems.Length; i++)
        {
            skinsItems[i].transform.localScale = Vector2.one;
            float dt = Mathf.Abs(skinsItems[i].transform.position.x - SkinsCenter.position.x);
            if (dt < min)
            {
                min = dt;
                minIndex = i;

            }
        }
        if (minIndex != -1)
        {
            currentItem = minIndex;
            CoreHandler.OnSkinScroll(currentItem);
        }
        posChanged = true;
        //Debug.Log ("SkinsUIStopDrag");
    }

}
