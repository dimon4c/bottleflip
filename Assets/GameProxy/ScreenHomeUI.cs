﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public partial class GameProxy : MonoBehaviour {
	//--------------------------------------------
	//Updates the following UI text
	//--------------------------------------------
	public Text HomeCoinsTxt;
	public Text HomeCoinsShadTxt;

	public Text HomeScoreTxt;
	public Text HomeScoreShadTxt;

	public Text [] HomeBestScoresTxt;
	public Text [] HomeBestScoresShadTxt;

	public GameObject HomeVideoBtn;

	//--------------------------------------------
	void HomeUIUpdateCoinsRotated(int from, int to, float time, float startDelay)
	{
		if (!HomeUI.activeSelf)
			return;
		StartCoroutine(IAddCounterRotated(from, to, HomeCoinsTxt, HomeCoinsShadTxt, time, startDelay));
	}
	//--------------------------------------------
	void HomeUIUpdateScoreRotated(int from, int to, float time, float startDelay)
	{
		if (!HomeUI.activeSelf)
			return;
		StartCoroutine(IAddCounterRotated(from, to, HomeScoreTxt, HomeScoreShadTxt, time, startDelay));
	}
	//--------------------------------------------
	void HomeUIUpdateCoins()
	{
		if (!HomeUI.activeSelf)
			return;
		HomeCoinsTxt.text = HomeCoinsShadTxt.text = Coins.ToString ();
	}
	//--------------------------------------------
	void HomeUIUpdateScore()
	{
		if (!HomeUI.activeSelf)
			return;
		HomeScoreTxt.text = HomeScoreShadTxt.text = Score.ToString ();
	}
	//--------------------------------------------
	void HomeUIUpdateBestScore()
	{
		if (!HomeUI.activeSelf)
			return;
		for (int i = 0; i < GameModeCount; i++) {
			HomeBestScoresTxt [i].text = BestScores [i].ToString ();
			HomeBestScoresShadTxt [i].text = BestScores [i].ToString ();
		}
	}
	//--------------------------------------------
	void HomeUIUpdateText () {
		if (!HomeUI.activeSelf)
			return;
		HomeUIUpdateCoins ();
		HomeUIUpdateScore ();
		HomeUIUpdateBestScore ();
	}
	//--------------------------------------------
	void HideHomeUI()
	{
		HomeUI.SetActive (false);
	}
	//--------------------------------------------
	void ShowHomeUI()
	{
		HomeUI.SetActive (true);
		HomeUIUpdateText ();
		if (!IsVideoLoaded ()) {
			LoadVideo ();
		}
	}

	//--------------------------------------------
	// UI Event Handlers
	//--------------------------------------------
	public void HomeUI_OnStartBtn(int mode) 
	{
		GameMode = mode;
		if (SkipSecondUIForward) {
			ShowGameUIAndStartLevel ();
			CoreHandler.OnLevelStarted (mode);
		} else {
			ShowSecondUI ();
		}

		HomeUI.SetActive (KeepHomeAfterStart);

	}
	//--------------------------------------------
	public void HomeUI_OnStoreBtn()
	{
		ShowStoreUI ();
	}
	//--------------------------------------------
	public void HomeUI_OnVideoBtn()
	{
		if (ShowVideo (VideoRequestor.HomeUI)) {
			HomeVideoBtn.SetActive (false);
		}
	}
	//--------------------------------------------
	public void HomeUI_OnLeaderboardBtn()
	{

	}
	//--------------------------------------------
	public void HomeUI_OnSoundDisableBtn()
	{
		Mute ();
	}
	//--------------------------------------------
	public void HomeUI_OnSoundEnableBtn()
	{
		Unmute ();
	}
	//--------------------------------------------

	//--------------------------------------------
	void OnHomeUIVideoLoaded()
	{
		HomeVideoBtn.SetActive (true);
	}
	//--------------------------------------------
	void OnHomeUIVideoWatched()
	{
		AddCoinsRotated (RewardAmount, 1, 0);
	}

	//--------------------------------------------

}
