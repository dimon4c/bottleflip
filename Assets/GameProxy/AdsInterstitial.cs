﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleMobileAds.Api;

public partial class GameProxy
{

    //--------------------------------------------
    public string AndroidInterstitialID;
    public string AndroidStaticInterstitialID;

    public string IOSInterstitialID;
    public string IOSStaticInterstitialID;
    //--------------------------------------------
    InterstitialAd interstitial;
    InterstitialAd staticInterstitial;
    //--------------------------------------------
    bool HasAds()
    {
        return (AdsRemoved == 0);
    }
    //--------------------------------------------
    void InitInterstitial()
    {
        FakeInterstitial.SetActive(false);
        FakeStaticInterstitial.SetActive(false);

        #if UNITY_EDITOR
        if (FakeAds)
        {
            return;
        }
        #endif

        if (HasAds())
        {
            LoadInterstitial();
            LoadStaticInterstitial();
        }
    }
    //---------------------------------------------------------------------------
    void LoadInterstitial()
    {
        if (HasAds())
        {
            #if UNITY_ANDROID
            interstitial = new InterstitialAd(AndroidInterstitialID);
            #endif

            #if UNITY_IOS
			interstitial = new InterstitialAd(IOSInterstitialID);
            #endif

            AdRequest request = new AdRequest.Builder().Build();
            interstitial.LoadAd(request);
        }
    }
    //---------------------------------------------------------------------------
    void LoadStaticInterstitial()
    {
        if (HasAds())
        {
            #if UNITY_ANDROID
            staticInterstitial = new InterstitialAd(AndroidStaticInterstitialID);
            #endif

            #if UNITY_IOS
			staticInterstitial = new InterstitialAd(IOSStaticInterstitialID);
            #endif

            AdRequest request = new AdRequest.Builder().Build();
            staticInterstitial.LoadAd(request);
        }
    }
    //---------------------------------------------------------------------------
    public void ShowInterstitial()
    {
        if (!HasAds())
            return;

        #if UNITY_EDITOR
        if (FakeAds)
        {
            FakeInterstitial.SetActive(true);
            return;
        }
        #endif

        if (interstitial.IsLoaded())
        {
            interstitial.Show();
        }
        LoadInterstitial();
    }
    //---------------------------------------------------------------------------
    public void ShowStaticInterstitial()
    {
        if (!HasAds())
            return;

        #if UNITY_EDITOR
        if (FakeAds)
        {
            FakeStaticInterstitial.SetActive(true);
            return;
        }
        #endif

        if (staticInterstitial.IsLoaded())
        {
            staticInterstitial.Show();
        }
        LoadStaticInterstitial();
    }
    //--------------------------------------------

}
