﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class GameProxy
{

    //--------------------------------------------
    public void StoreUI_OnCloseBtn()
    {
        HideStoreUI();
    }
    //--------------------------------------------
    public void ShowStoreUI()
    {
        StoreUI.SetActive(true);
    }
    //--------------------------------------------
    public void HideStoreUI()
    {
        StoreUI.SetActive(false);
    }
    //--------------------------------------------
}
