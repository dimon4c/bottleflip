﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public partial class GameProxy : MonoBehaviour
{
	
    //--------------------------------------------
    // Coins
    //--------------------------------------------
    public void SetCoins(int coins)
    {
        Coins = coins;
        UpdateAllCoins();
    }
    //--------------------------------------------
    public void AddCoins(int coins)
    {
        Coins += coins;
        UpdateAllCoins();
    }
    //--------------------------------------------
    public void AddCoinsRotated(int coins, float time, float startDelay)
    {
        UpdateAllCoinsRotated(Coins, coins, time, startDelay);
        Coins += coins;
    }

    public void AddCoinsInGame(int coins, float time, float startDelay)
    {
        if (DoubleCoins == 1)
            coins *= 2;
        UpdateAllCoinsRotated(Coins, coins, time, startDelay);
        Coins += coins;
    }
    //--------------------------------------------
    public void AddCoinseFlying(int coins)
    {
        /*
		UpdateAllScoreRotated (Score, score, time, startDelay);
		Score += score;
		CheckMaxLevelScore ();
		CheckBestScore ();
		*/

        //todo...

    }
    //--------------------------------------------
    public bool SpendCoins(int coins)
    {
        if (Coins >= coins)
        {
            Coins -= coins;
            UpdateAllCoins();
            return true;
        }
        else
        {
            return false;
        }
    }

    //--------------------------------------------
    // Score
    //--------------------------------------------
    public int GetScore()
    {
        return Score;
    }

    public void SetScore(int score)
    {
        Score = score;
        CheckMaxLevelScore();
        CheckBestScore();
        UpdateAllScore();
    }
    //--------------------------------------------
    public void AddScore(int score)
    {
        Score += score;
        CheckMaxLevelScore();
        CheckBestScore();
        UpdateAllScore();
    }
    //--------------------------------------------
    public void AddScoreRotated(int score, float time, float startDelay)
    {
        UpdateAllScoreRotated(Score, score, time, startDelay);
        Score += score;
        CheckMaxLevelScore();
        CheckBestScore();

    }

    //--------------------------------------------
    public void ResetScore()
    {
        Score = MaxLevelScore = 0;
        UpdateAllScore();
    }
    //--------------------------------------------

    public void EndLevel()
    {
        GameUIEndLevel();
    }
    //--------------------------------------------

    public void ShowMessage(string text)
    {
        ObjectShowMessage(text);
    }

    //--------------------------------------------
    public bool CanUserMove()
    {
//        IsRunning blocks throws after revive
        if (IsRunning && GameUI.activeSelf &&
            (!(PauseUI.activeSelf || GameOverUI.activeSelf || StoreUI.activeSelf)))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public bool CanUserMoveTest()
    {
        //        IsRunning blocks throws after revive
        if (GameUI.activeSelf &&
            (!(PauseUI.activeSelf || GameOverUI.activeSelf || StoreUI.activeSelf)))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    //--------------------------------------------


}
