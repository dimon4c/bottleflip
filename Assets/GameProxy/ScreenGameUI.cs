﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public partial class GameProxy : MonoBehaviour
{

    //--------------------------------------------
    public Text GameCoinsTxt;
    public Text GameCoinsShadTxt;
    public Text GameScoreTxt;
    public Text GameScoreShadTxt;
    public Text GameBestScoreTxt;
    public Text GameBestScoreShadTxt;
    public GameObject GameCoinsXIcon;
    public GameObject GameVideoBtn;
    //--------------------------------------------
    //Service variables
    int RevivedCount = 0;
    int CoinsXTemp = 1;
    float ActiveTime = 0;
    bool IsRunning = false;
    int GamesPlayed = 0;
    //--------------------------------------------
    void GameUIUpdateCoinsRotated(int from, int to, float time, float startDelay)
    {
        if (!GameUI.activeSelf)
            return;
        StartCoroutine(IAddCounterRotated(from, to, GameCoinsTxt, GameCoinsShadTxt, time, startDelay));
    }
    //--------------------------------------------
    void GameUIUpdateScoreRotated(int from, int to, float time, float startDelay)
    {
        if (!GameUI.activeSelf)
            return;
        StartCoroutine(IAddCounterRotated(from, to, GameScoreTxt, GameScoreShadTxt, time, startDelay));
    }
    //--------------------------------------------
    void GameUIUpdateCoins()
    {
        GameCoinsTxt.text = GameCoinsShadTxt.text = Coins.ToString();
    }
    //--------------------------------------------
    void GameUIUpdateScore()
    {
        GameScoreTxt.text = GameScoreShadTxt.text = Score.ToString();
    }
    //--------------------------------------------
    void GameUIUpdateBestScore()
    {
        GameBestScoreTxt.text = BestScores[GameMode].ToString();
        GameBestScoreShadTxt.text = BestScores[GameMode].ToString();
    }
    //--------------------------------------------
    void GameUIUpdate()
    {
        GameUIUpdateCoins();
        GameUIUpdateScore();
        GameUIUpdateBestScore();
    }
    //--------------------------------------------
    void HideGameUI()
    {
        GameUI.SetActive(false);
        IsRunning = false;
    }
    //--------------------------------------------
    void ShowGameUIAndStartLevel()
    {
        IsRunning = true;

        ResetScore();
        RevivedCount = 0;
        GameUI.SetActive(true);
        GameUIUpdate();

        CoinsXTemp = 1;
        if (CoinsXPerm > 1 || CoinsXTemp > 1)
        {
            ShowXCoinsIcon();
        }
        else
        {
            HideXCoinsIcon();
        }
    }
    //--------------------------------------------
    void ReviveGame()
    {
        RevivedCount++;
        CoreHandler.OnLevelRevived(GameMode);
        IsRunning = true;
    }
    //--------------------------------------------
    void ResumeGameUI()
    {
        CoreHandler.OnLevelResumed();
        IsRunning = true;
    }

    //--------------------------------------------
    // UI Event Handlers
    //--------------------------------------------
    public void GameUI_OnPauseBtn()
    {
        if (!PauseUI.activeSelf)
        {
            ShowPauseUI();
            CoreHandler.OnLevelPaused();
            IsRunning = false;
        }
        else
        {
            HidePauseUI();
            CoreHandler.OnLevelResumed();
            IsRunning = true;
        }

    }
    //--------------------------------------------
    public void GameUI_OnVideoBtn()
    {
        if (ShowVideo(VideoRequestor.GameUI))
        {
            GameVideoBtn.SetActive(false);
        }
    }
    //--------------------------------------------


    //--------------------------------------------
    // Commands expected from core logic
    //--------------------------------------------
    public void GameUIEndLevel()
    {

        IsRunning = false;
        GamesPlayed++;

        if (!IsVideoLoaded())
        {
            LoadVideo();
        }
			
        if (RevivedCount < MaxRevivesCount)
        {
			
            CoreHandler.OnReviveProposed();
            ShowGameOverUI();

            if (!CheckRate())
            {
                ShowStaticInterstitial();
            }

        }
        else
        {

            if (!KeepGameAfterEnd)
            {
                HideGameUI();
            }

            CoinsXTemp = 1;
            HideXCoinsIcon();

            if (SkipSecondUIBackward)
            {
                ShowHomeUI();
            }
            else
            {
                ShowSecondUI();
            }

            CoreHandler.OnLevelEnded();

            if (!CheckRate())
            {
                ShowInterstitial();
            }
        }

        SaveAllPreferences();
        ReportBestScore();
			
    }
    //--------------------------------------------
    void OnGameUIVideoLoaded()
    {
        if (CoinsXModeEnabled && CoinsXPerm == 1 && CoinsXTemp == 1)
        {
            //GameVideoBtn.SetActive (true);
        }
    }
    //--------------------------------------------
    void OnGameUIVideoWatched()
    {
        if (CoinsXModeEnabled)
        {
            CoinsXTemp = CoinsXMultiplier;
            if (CoinsXTemp > 1)
            {
                ShowXCoinsIcon();
            }
        }
    }
    //--------------------------------------------
    void ShowXCoinsIcon()
    {
        GameCoinsXIcon.SetActive(true);
    }
    //--------------------------------------------
    void HideXCoinsIcon()
    {
        GameCoinsXIcon.SetActive(false);
    }
    //--------------------------------------------
    void GameUIOnUpdate()
    {
        if (IsRunning)
        {
            ActiveTime += Time.deltaTime;
        }
    }
    //--------------------------------------------
    void OnApplicationFocus(bool willFocus)
    {
        if (willFocus)
        {
            //will resume now
            //put save state
        }
        else
        {
            //will pause now
            //check save state
        }

        IsRunning = willFocus;
    }
    //--------------------------------------------
    void OnApplicationPause(bool willPause)
    {
        if (willPause)
        {
            //will pause now
            //check save state
        }
        else
        {
            //will resume now
            //put save state
        }

        IsRunning = !willPause;
    }
    //--------------------------------------------
}
