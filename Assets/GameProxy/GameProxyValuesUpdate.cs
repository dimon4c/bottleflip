﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public partial class GameProxy {

	//--------------------------------------------
	// Coins
	//--------------------------------------------
	void UpdateAllCoins()
	{
		HomeUIUpdateCoins ();
		SecondUIUpdateCoins ();
		GameUIUpdateCoins ();
	}
	//--------------------------------------------
	void UpdateAllCoinsRotated(int from, int to, float time, float startDelay)
	{
		HomeUIUpdateCoinsRotated (from, to, time, startDelay);
		SecondUIUpdateCoinsRotated (from, to, time, startDelay);
		GameUIUpdateCoinsRotated (from, to, time, startDelay);
	}

	//--------------------------------------------
	// Score
	//--------------------------------------------
	void UpdateAllScore()
	{
		HomeUIUpdateScore ();
		SecondUIUpdateScore ();
		GameUIUpdateScore ();
	}
	//--------------------------------------------
	void UpdateAllScoreRotated(int from, int to, float time, float startDelay)
	{
		HomeUIUpdateScoreRotated (from, to, time, startDelay);
		SecondUIUpdateScoreRotated (from, to, time, startDelay);
		GameUIUpdateScoreRotated (from, to, time, startDelay);
	}
	//--------------------------------------------


	//--------------------------------------------
	// Best Score
	//--------------------------------------------
	void UpdateAllBestScore()
	{
		HomeUIUpdateBestScore ();
		SecondUIUpdateBestScore ();
	}
	//--------------------------------------------
	public void CheckBestScore () {
		if (BestScores [GameMode] < Score) {
			BestScores [GameMode] = Score;
		}
		UpdateAllBestScore ();
	}
	//--------------------------------------------


	//--------------------------------------------
	// Max Score
	//--------------------------------------------
	public void CheckMaxLevelScore () {
		if (MaxLevelScore < Score) {
			MaxLevelScore = Score;
		}
	}
	//--------------------------------------------


	//-----------------------------------------------------------
	IEnumerator IAddCounterRotated(int startValue, int needToAdd, Text text, Text textShad, float execTime = 1f, float startingDelay = 0f)
	{
		yield return new WaitForSeconds(startingDelay);

		int added = startValue;
		int step = 1;
		int last = 0;
		int max = startValue + needToAdd;

		if (needToAdd > 100) {
			step = needToAdd / 10;
			last = needToAdd % 10;
			max = needToAdd - last;
		}

		float delay = execTime / (needToAdd / step);

		for (int i = startValue; i < max; i += step)
		{
			added += step;
			text.text = added.ToString ();
			textShad.text = added.ToString ();
			yield return new WaitForSeconds(delay);
		}

		for (int i = 0; i < last; i++)
		{
			added ++;
			text.text = added.ToString ();
			textShad.text = added.ToString ();
			yield return new WaitForSeconds(delay);
		}

	}

}
