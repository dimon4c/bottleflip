﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public partial class GameProxy
{
    //--------------------------------------------
    //Updates the following UI text
    //--------------------------------------------
    public Text SecondCoinsTxt;
    public Text SecondCoinsShadTxt;

    public Text SecondScoreTxt;
    public Text SecondScoreShadTxt;

    public Text[] SecondBestScoresTxt;
    public Text[] SecondBestScoresShadTxt;

    public GameObject SecondVideoBtn;

    //--------------------------------------------
    void SecondUIUpdateCoinsRotated(int from, int to, float time, float startDelay)
    {
        if (!SecondUI.activeSelf)
            return;
        StartCoroutine(IAddCounterRotated(from, to, SecondCoinsTxt, SecondCoinsShadTxt, time, startDelay));
    }
    //--------------------------------------------
    void SecondUIUpdateScoreRotated(int from, int to, float time, float startDelay)
    {
        if (!SecondUI.activeSelf)
            return;
        StartCoroutine(IAddCounterRotated(from, to, SecondScoreTxt, SecondScoreShadTxt, time, startDelay));
    }
    //--------------------------------------------
    void SecondUIUpdateCoins()
    {
        SecondCoinsTxt.text = SecondCoinsShadTxt.text = Coins.ToString();
    }
    //--------------------------------------------
    void SecondUIUpdateScore()
    {
        SecondScoreTxt.text = SecondScoreShadTxt.text = Score.ToString();
    }
    //--------------------------------------------
    void SecondUIUpdateBestScore()
    {
        for (int i = 0; i < GameModeCount; i++)
        {
            SecondBestScoresTxt[i].text = BestScores[i].ToString();
            SecondBestScoresShadTxt[i].text = BestScores[i].ToString();
        }
    }
    //--------------------------------------------
    void SecondUIUpdate()
    {
        SecondUIUpdateCoins();
        SecondUIUpdateScore();
        SecondUIUpdateBestScore();
    }
    //--------------------------------------------
    void HideSecondUI()
    {
        //custom code
        HideSkinsUI();
        //end...

        SecondUI.SetActive(false);
    }
    //--------------------------------------------
    void ShowSecondUI()
    {
        if (SkipSecondUIForward && SkipSecondUIBackward)
        {
            throw new UnityException();
        }

        SecondUI.SetActive(true);

        //custom code
        ShowSkinsUI();
        //end...

        SecondUIUpdate();
    }

    //--------------------------------------------
    // UI Event Handlers
    //--------------------------------------------
    public void SecondUI_OnStartBtn(int mode)
    {
        GameMode = mode;
        //custom code
        HideSkinsUI();
        SecondUI.SetActive(false);
        //end...
        //SecondUI.SetActive (KeepSecondAfterStart);
        ShowGameUIAndStartLevel();
        CoreHandler.OnLevelStarted(mode);

    }
    //--------------------------------------------
    public void SecondUI_OnHomeBtn()
    {
        HideSecondUI();
        ShowHomeUI();
    }
    //--------------------------------------------
    public void SecondUI_OnStoreBtn()
    {
        
        ShowStoreUI();
    }
    //--------------------------------------------
    public void SecondUI_OnVideoBtn()
    {
        if (ShowVideo(VideoRequestor.SecondUI))
        {
            SecondVideoBtn.SetActive(false);
        }
    }
    //--------------------------------------------
    void OnSecondUIVideoLoaded()
    {
        SecondVideoBtn.SetActive(true);
    }
    //--------------------------------------------
    void OnSecondUIVideoWatched()
    {
        AddCoinsRotated(RewardAmount, 1, 0);
    }
    //--------------------------------------------
}
