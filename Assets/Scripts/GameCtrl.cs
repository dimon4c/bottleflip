﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Security.Cryptography;

public partial class Game : MonoBehaviour
{
    public List<Thrower> bottles;
    public Thrower thObject;
    public GameObject floorPrefab;
    public SpriteRenderer table;
    List<BoxCollider> floorCollider = new List<BoxCollider>();
    Vector3 startPos, endPos;
    float multiplier = 1;
    float seconds = 0;
    bool calcTime = false;
    bool cantMove = false;
    int currentTurn = 0;
    bool startCheck = false;
    Vector3 cameraDefault;
    Vector3 defthrowScale = new Vector3(0.5f, 0.5f, 0.03f);
    Vector3[] throwScales = new Vector3[]{ new Vector3(0.5f, 1f, 1f), new Vector3(0.33f, 1f, 1f), new Vector3(0.2f, 1f, 1f), new Vector3(0.1f, 1f, 1f) };
    public List<Sprite> platformSkins;
    int throwIndex = 0;
    bool paused = false;
    IEnumerator MoverFloor;
    bool waitingKeyDown = true;
    bool reduce = false;
    Vector3 animPos;

    void Revive()
    {
        if (thObject.game == GameType.Throw)
            StartCoroutine(MoverFloor);
        if (thObject.game != GameType.Jump)
            thObject.ResetPos();
        else
            thObject.ResetPos(floorCollider[0].gameObject.transform.position + new Vector3(0f, 0.1f));
        waitingKeyDown = true;
        calcTime = false;
        cantMove = false;
        startCheck = false;
        bool paused = false;
        multiplier = 1;
        seconds = 0;
        thObject.gameObject.SetActive(true);
        table.enabled = true;
    }

    void EndGame()
    {
        while (floorCollider.Count > 0)
        {
            GameObject toRemove = floorCollider[0].gameObject;
            floorCollider.RemoveAt(0);
            Destroy(toRemove);
        }
        if (thObject.game != GameType.Flip && MoverFloor != null)
            StopCoroutine(MoverFloor);
        thObject.gameObject.SetActive(false);
        thObject.throwing = false;
        multiplier = 1;
        seconds = 0;
        throwIndex = 0;
//        currentTurn = 0;
        calcTime = false;
        cantMove = false;
        currentTurn = 0;
        paused = false;
        startCheck = false;
        waitingKeyDown = true;
    }

    void SetFloorSprite()
    {
        if (platformSkins[throwIndex] != null)
        {
            floorCollider[1].gameObject.GetComponentInChildren<SpriteRenderer>().sprite = platformSkins[throwIndex];
            if (thObject.game == GameType.Jump && currentTurn > 5)
            {
                floorCollider[1].gameObject.GetComponentInChildren<SpriteRenderer>().sprite = platformSkins[Random.Range(0, platformSkins.Count)];
            }
            Vector3 s = floorCollider[1].gameObject.transform.localScale;
            Vector3 s1 = floorCollider[1].gameObject.transform.transform.GetChild(0).localScale;

//            floorCollider[1].gameObject.transform.GetChild(0).localScale = new Vector3(2f / s.x, 35f / s.y, 1f / s.z);
            Vector3 S = platformSkins[throwIndex].bounds.size;
            floorCollider[1].size = new Vector3(S.x / s.x, floorCollider[1].size.y, floorCollider[1].size.z); 
            if (!reduce)
                throwIndex++;
            else
                throwIndex--;
            if (throwIndex >= platformSkins.Count)
                reduce = true;
            else if (throwIndex < 0)
            {
                throwIndex = 0;
                reduce = false;
            }
            
        }
        floorCollider[1].enabled = false;
    }

    void GameOver()
    {
//        table.enabled = false;
        if (floorCollider.Count > 1)
        {
            if (floorCollider[1] != null)
                floorCollider[1].gameObject.GetComponent<FloorLogic>().collided = false;
        }
        startCheck = false;
        seconds = 0;
        calcTime = false;
//        cantMove = false;
        cantMove = false;
        waitingKeyDown = true;
        thObject.throwing = false;
        if (GameProxy.Instance.GetScore() == 0)
        {
            paused = false;  
            thObject.ResetPos();
        }
        else
        {
            thObject.gameObject.SetActive(false);
            if (thObject.game == GameType.Throw && MoverFloor != null)
                StopCoroutine(MoverFloor);
            GameProxy.Instance.EndLevel();
        }

    }

    void CreateLevel(int mode)
    {
        coinAnim.SetActive(false);
        speed = 0.01f;
        manAinm.transform.position = animPos;
        table.enabled = true;
        thObject = bottles[currentSkin];
//        cantMove = true;
        Camera.main.transform.position = cameraDefault;
        thObject.ResetPos();
        thObject.gameObject.SetActive(true);
        thObject.gameObject.transform.position = Vector3.zero + new Vector3(0.07f, 0.3f);


        thObject.game = (GameType)mode;
        CollisionDetectionMode col = (GameType)mode == GameType.Flip ? CollisionDetectionMode.ContinuousDynamic : CollisionDetectionMode.Discrete;
        thObject.SetCollision(col);
        CreateFloor(mode);
//        cantMove = false;
    }

    void CreateFloor(int mode)
    {
        GameObject go1, go2;
        switch (mode)
        {
            case 0: //throw
                go1 = Instantiate(floorPrefab);
                go2 = Instantiate(floorPrefab);
                go1.SetActive(true);
                go2.SetActive(true);
                floorCollider.Add(go1.GetComponent<BoxCollider>());
                floorCollider.Add(go2.GetComponent<BoxCollider>());
                go1.transform.position = Vector3.zero + new Vector3(0f, 0.11f);
                go2.transform.position = Vector3.zero + new Vector3(0f, 0.75f);
                paused = false;
                MoverFloor = IMoveFloorContinuous(floorCollider[1].gameObject);
                StartCoroutine(MoverFloor); 
                go1.transform.position = Vector3.zero + new Vector3(0.073f, 0.10f);
                go1.transform.localScale = new Vector3(1.2f, 0.5f, 0.03f);
                SetFloorSprite();
                break;
            case 1: //jump
                go1 = Instantiate(floorPrefab);
                go2 = Instantiate(floorPrefab);
                go1.SetActive(true);
                go2.SetActive(true);
                floorCollider.Add(go1.GetComponent<BoxCollider>());
                floorCollider.Add(go2.GetComponent<BoxCollider>());
                go1.transform.position = Vector3.zero + new Vector3(0.073f, 0.10f);

                go2.transform.position = Vector3.zero + new Vector3(0f, Random.Range(0.6f, 0.8f));
                go1.transform.localScale = new Vector3(1.2f, 0.5f, 0.03f);
                SetFloorSprite();
                break;
            case 2: //flip
                go1 = Instantiate(floorPrefab);
                go1.SetActive(true);
                floorCollider.Add(go1.GetComponent<BoxCollider>());
                go1.transform.position = Vector3.zero + new Vector3(0.073f, 0.10f);
                go1.transform.localScale = new Vector3(1.2f, 0.5f, 0.03f);
                break;

        }
        floorCollider[0].transform.GetChild(0).gameObject.SetActive(false);
       
    }

    void CheckGO()
    {
        if (thObject.gameObject.transform.position.y < floorCollider[0].gameObject.transform.position.y)
        {
           
            GameOver();
        }
    }

    public void ColliderStatus()
    {
       
//        CheckGO();
        if (thObject.game != GameType.Flip)
        {
//            if (thObject.gameObject.transform.position.y > floorCollider[1].gameObject.transform.position.y + 0.036f)
            if (thObject.rb.velocity.y < 0)
            {
                floorCollider[1].enabled = true;


            }
            if (Vector3.Distance(floorCollider[1].gameObject.transform.position, thObject.gameObject.transform.position) > Vector3.Distance(floorCollider[0].gameObject.transform.position, thObject.gameObject.transform.position))
            {
                floorCollider[1].enabled = false;
            }
        }
//        Invoke("NextTurnHandler", 0.1f);

    }

    //    bool unfrozen = false;

    IEnumerator Invoker(int turn, float waitTime)
    {
//        yield return new WaitForSeconds(waitTime);
        yield return new WaitForFixedUpdate();
        yield return new WaitWhile(() => !thObject.IsStanding());
       
        yield return new WaitForFixedUpdate();
       
        if (turn == currentTurn)
            NextTurnHandler();
    }

    void NextTurnHandler()
    {
        cantMove = true;
        startCheck = false;
        if (thObject.game != GameType.Flip)
        {
            if (!thObject.IsStanding())
            {
                if (floorCollider[0].gameObject.transform.position.y > thObject.gameObject.transform.position.y)
                    return;
                StartCoroutine(Invoker(currentTurn, 0.2f));
                return;
            }
            else if (Vector3.Distance(floorCollider[0].gameObject.transform.position, thObject.gameObject.transform.position) < Vector3.Distance(floorCollider[1].gameObject.transform.position, thObject.gameObject.transform.position))
            {
                currentTurn++;
                GameOver();
                return;
            }
        }
       
        switch (thObject.game)
        {
            case GameType.Throw: //Throw
                if (floorCollider[1].gameObject.GetComponent<FloorLogic>().collided)
                {
                    
                    cantMove = true;

                   
                    ThrowSwitch();
//                    Invoke("ThrowSwitch", 0.3f);
                    

                }
               
                break;
            case GameType.Jump: //Jump
                if (floorCollider[1].gameObject.GetComponent<FloorLogic>().collided)
                {
                    cantMove = true;
                    if (MoverFloor != null)
                        StopCoroutine(MoverFloor);
//                    thObject.rb.isKinematic = true;
                    JumpSwitch();
//                    Invoke("JumpSwitch", 0.3f);
//                    thObject.rb.isKinematic = false;

                }
                else
                {
                    startCheck = true;
                }
                break;
            case GameType.Flip: //Flip
                cantMove = true;
//                Invoke("FlipSwitch", 0.3f);
                FlipSwitch();
                break;
        }

        currentTurn++;
       


    }

    public GameObject manAinm;

    void ThrowSwitch()
    {
        
        float currentAngle = thObject.CurentAngle();
        if (currentAngle >= 0f && currentAngle <= 15f || currentAngle >= 165f && currentAngle <= 180f)
        {
            if (Vector3.Distance(floorCollider[0].gameObject.transform.position, thObject.gameObject.transform.position) < Vector3.Distance(floorCollider[1].gameObject.transform.position, thObject.gameObject.transform.position))
            {
                GameOver();
                return;
            }
            AddCoins(throwIndex + 1);
            AddScore(1);
            floorCollider[1].gameObject.GetComponent<FloorLogic>().collided = false;

//            float XScale = floorCollider[1].gameObject.transform.localScale.y - 0.1f;
          

       
         
//            floorCollider[1].gameObject.transform.localScale = newScale;
//        MoveFloor(floorCollider[1].gameObject, newPos);
            thObject.ResetPos();
            thObject.transform.rotation = thObject.defRot;
            thObject.freeze = true;
            thObject.freezeX = true;
            thObject.rb.angularVelocity = Vector3.zero;
            if (Random.Range(0, 2) == 0) //change pos
            {
                thObject.rb.isKinematic = true;
                Vector3 newPos = new Vector3(thObject.transform.position.x, 0.15f, 0);
                newPos.x = Random.Range(-0.3f, 0.3f);
                thObject.transform.position = newPos;
                thObject.rb.isKinematic = false;
            }
            if (Random.Range(0, 2) == 0) //if moving change speed
            {
                paused = true;
                speed = Random.Range(0.005f, 0.015f);
                paused = false;
            }
            else
            {
                paused = true;
                float randomX = Random.Range(-0.35f, 0.35f);
                float randomY = floorCollider[1].gameObject.transform.position.y;
              
                floorCollider[1].gameObject.transform.position = new Vector3(randomX, randomY, 0);

            }
//            paused = false;
            cantMove = false;

            SetFloorSprite();
            if (GameProxy.Instance.GetScore() % 3 == 0 && GameProxy.Instance.GetScore() != 0 && GameProxy.Instance.CanUserMove())
            {
                manAinm.SetActive(false);
                manAinm.SetActive(true);
            }

        }
        else
        {
            Debug.Log("Bottle isn't on it's bottom or cap");
            GameOver();
        }
    }

    void MoveFloor(GameObject go, Vector3 destination)
    {
        StartCoroutine(IMoveFloor(go, destination));
    }

    IEnumerator IMoveFloorContinuous(GameObject go)
    {

        Vector3 rightBorder = new Vector3(0.4f, go.transform.position.y, go.transform.position.z);
        Vector3 leftBorder = new Vector3(-0.27f, go.transform.position.y, go.transform.position.z);
        float step = 0.008f;
        while (true)
        {

            while (Vector3.Distance(go.transform.position, rightBorder) > speed)
            {
                //            Debug.Log("moving");
                go.transform.position = Vector3.MoveTowards(go.transform.position, rightBorder, speed);
                yield return new WaitWhile(() => paused == true);
                yield return new WaitForEndOfFrame();
            }
            while (Vector3.Distance(go.transform.position, leftBorder) > step)
            {
                //            Debug.Log("moving");
                go.transform.position = Vector3.MoveTowards(go.transform.position, leftBorder, speed);
                yield return new WaitWhile(() => paused == true);
                yield return new WaitForEndOfFrame();
            }
        }
    }

    float speed = 0.01f;

    IEnumerator IMoveFloor(GameObject go, Vector3 destination)
    {
//        Debug.Log("moving");
       
        while (Vector3.Distance(go.transform.position, destination) > speed)
        {
//            Debug.Log("moving");
            go.transform.position = Vector3.MoveTowards(go.transform.position, destination, speed);
            yield return new WaitForEndOfFrame();
        }
        cantMove = false;

    }

    void JumpSwitch()
    {
        float currentAngle = thObject.CurentAngle();
        if (currentAngle >= 0f && currentAngle <= 15f || currentAngle >= 165f && currentAngle <= 180f)
        {
            if (Vector3.Distance(floorCollider[0].gameObject.transform.position, thObject.gameObject.transform.position) < Vector3.Distance(floorCollider[1].gameObject.transform.position, thObject.gameObject.transform.position))
            {
                GameOver();
                return;
            }
            thObject.transform.rotation = thObject.defRot;
            AddCoins(throwIndex + 1);
            AddScore(1);
            CameraControl();
            manAinm.transform.position = floorCollider[1].gameObject.transform.position + new Vector3(0f, 0.2f, 1f);
            GameObject toRemove = floorCollider[0].gameObject;
            floorCollider.RemoveAt(0);
            Destroy(toRemove);
//        floorCollider[0].enabled = true;
            GameObject go1 = Instantiate(floorPrefab);
            go1.SetActive(true);
            floorCollider.Add(go1.GetComponent<BoxCollider>());
            go1.transform.position = floorCollider[0].gameObject.transform.position + new Vector3(0f, 0.5f);
            float randomX = Random.Range(-0.4f, 0.5f);
            float randomY = floorCollider[1].gameObject.transform.position.y;
            randomY += Random.Range(0f, 0.4f);
            floorCollider[1].gameObject.transform.position = new Vector3(randomX, randomY, 0);
            floorCollider[0].enabled = true;
           
            SetFloorSprite();
            if (currentTurn == 11)
            {
                MoverFloor = IMoveFloorContinuous(floorCollider[1].gameObject);
                StartCoroutine(MoverFloor); 
            }
            if (currentTurn - 11 > 0 && Random.Range(0, 5) == 0)
            {
                if (MoverFloor != null)
                    StopCoroutine(MoverFloor);
                MoverFloor = IMoveFloorContinuous(floorCollider[1].gameObject);
                StartCoroutine(MoverFloor); 
            }
            if (GameProxy.Instance.GetScore() % 3 == 0 && GameProxy.Instance.GetScore() != 0 && GameProxy.Instance.CanUserMove())
            {
                manAinm.SetActive(false);
                manAinm.SetActive(true);
            }
        }
        else
        {
            Debug.Log("Bottle isn't on it's bottom or cap");
            GameOver();
        }
//        thObject.ResetPos(floorCollider[0].gameObject.transform.position + new Vector3(0f, 0.1f));
//        cantMove = false;

    }

    public GameObject coinAnim;

    void AddCoins(int money)
    {
        coinAnim.SetActive(false);
        coinAnim.SetActive(true);
        GameProxy.Instance.AddCoinsInGame(money, 0.5f, 0);
    }

    void AddScore(int score)
    {
        GameProxy.Instance.AddScoreRotated(score, 0.5f, 0);
    }

    void FlipSwitch()
    {
        float currentAngle = thObject.CurentAngle();
        if (currentAngle >= 0f && currentAngle <= 10f) //it's on bottom with small dispersion
        {
            thObject.transform.rotation = thObject.defRot;
            AddCoins(1);
            AddScore(1);
            switch (currentTurn)
            {
                case 1:
                    AddCoins(1);
                    break;
                case 5:
                    AddCoins(5);
                    break;
                case 10:
                    AddCoins(10);
                    break;
                case 20:
                    AddCoins(20);
                    break;
                case 25:
                    AddCoins(25);
                    break;
                case 33:
                    AddCoins(33);
                    break;
                case 50:
                    AddCoins(50);
                    break;
                case 66:
                    AddCoins(66);
                    break;
                case 75:
                    AddCoins(75);
                    break;
                case 100:
                    AddCoins(100);
                    break;
            }
            if (GameProxy.Instance.GetScore() % 3 == 0 && GameProxy.Instance.GetScore() != 0 && GameProxy.Instance.CanUserMove())
            {
                manAinm.SetActive(false);
                manAinm.SetActive(true);
            }
        }
        else if (currentAngle >= 170f && currentAngle <= 180f) //it's on the cap with small dispersion
        {
            thObject.freeze = true;
            thObject.freezeX = true;
            thObject.transform.rotation = thObject.defRot;
            thObject.rb.angularVelocity = Vector3.zero;
            AddCoins(3);
            if (currentSkin != 7 && currentSkin != 6)
                AddScore(5);
            else
                AddScore(1);
            switch (currentTurn)
            {
                case 1:
                    AddCoins(1);
                    break;
                case 5:
                    AddCoins(5);
                    break;
                case 10:
                    AddCoins(10);
                    break;
                case 20:
                    AddCoins(20);
                    break;
                case 25:
                    AddCoins(25);
                    break;
                case 33:
                    AddCoins(33);
                    break;
                case 50:
                    AddCoins(50);
                    break;
                case 66:
                    AddCoins(66);
                    break;
                case 75:
                    AddCoins(75);
                    break;
                case 100:
                    AddCoins(100);
                    break;
            }
//            thObject.ResetPos();
//            thObject.freeze = false;
//            thObject.freezeX = false;
            if (GameProxy.Instance.GetScore() % 3 == 0 && GameProxy.Instance.GetScore() != 0 && GameProxy.Instance.CanUserMove())
            {
                manAinm.SetActive(false);
                manAinm.SetActive(true);
            }


        }
        else //it's not on the bottom nor cap
        {
            Debug.Log("Bottle isn't on it's bottom or cap");
            GameOver();
        }
        cantMove = false;
    }

    public void CameraControl()
    {
        Vector3 newCamPos = floorCollider[1].transform.position;
        newCamPos.y += 0.5f;
        newCamPos.x = Camera.main.transform.position.x;
        newCamPos.z = Camera.main.transform.position.z;
//        Camera.main.transform.position = newCamPos;
        MoveCamera(newCamPos);
    }

    void MoveCamera(Vector3 destination)
    {
        StartCoroutine(IMoveCamera(destination));
    }

    IEnumerator IMoveCamera(Vector3 destination)
    {
        while (Vector3.Distance(Camera.main.transform.position, destination) > 0.01f)
        {
            //            Debug.Log("moving");
            Camera.main.transform.position = Vector3.MoveTowards(Camera.main.transform.position, destination, 0.02f);
            yield return new WaitForEndOfFrame();
        }
        cantMove = false;
    }
    //*/
    void FixedUpdate()
    {
        //*/
        if (!GameProxy.Instance.CanUserMove())
        {
//            Debug.Log("Ignoring");
            return;
        }
        //*/
//        thObject.CurentAngle();
        if (!thObject.IsStanding() && startCheck)
            ColliderStatus();
        if (thObject.isFalling())
            CheckGO();
        if (startCheck && thObject.game != GameType.Flip)
        {
            if (floorCollider[1].gameObject.GetComponent<FloorLogic>().collided)
            {
                paused = true;

                thObject.rb.velocity = Vector3.zero;
                thObject.freeze = false;
            }
            
//            Debug.Log("Paused");
//            Debug.Log(thObject.IsStanding() && startCheck);
        }
        if (!cantMove && thObject.IsStanding() && startCheck)
        {
                
//            Debug.Log("NextTurnHandler");

            cantMove = true;
            startCheck = false;
//            NextTurnHandler();
            StartCoroutine(Invoker(currentTurn, 0f));
//            NextTurnHandler();
            
        }
       
    }

    void StartCheckDelay()
    {
        StartCoroutine(IStartCheck());
    }

    void StartCheckDelayStand()
    {
        StartCoroutine(IStartCheckStand());
    }

    IEnumerator IStartCheckStand()
    {
        yield return new   WaitUntil(thObject.IsStanding);
        startCheck = true;
    }

    IEnumerator IStartCheck()
    {
        yield return new   WaitUntil(() => !thObject.IsStanding());
        startCheck = true;
    }
    //*/
    int fc = 0;

    void Update()
    {
        table.enabled = GameProxy.Instance.CanUserMove();
//        paused = !GameProxy.Instance.CanUserMove();
        fc++;
        if (!GameProxy.Instance.CanUserMove() || !thObject.gameObject.activeSelf || !thObject.IsStanding() || cantMove)
        {
//            Debug.Log("Ignoring " + !thObject.IsStanding());
            return;
        }
       
        if (Input.GetMouseButtonDown(0))
        {
            waitingKeyDown = false;
            CallTimer();
            startPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            thObject.OnMouseDownDo();
        }
        if (Input.GetMouseButtonUp(0))
        {
            
            //*/
            if (!waitingKeyDown)
            {
                waitingKeyDown = true;
                calcTime = false;
                endPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
//            multiplier = CalcSpeed() >= 1 ? CalcSpeed() : 1;
                thObject.OnMouseUpDo(startPos, endPos, seconds);
                StartCheckDelay();
            }
            //*/
        }

    }

    void CallTimer()
    {
        if (!calcTime)
            StartCoroutine(CalcTime());
    }

    float CalcSpeed()
    {
        return Vector3.Distance(startPos, endPos) / seconds;
    }

    IEnumerator CalcTime()
    {
        seconds = 0;
        calcTime = true;
        do
        {
            seconds++;
            yield return new WaitForSecondsRealtime(1f);
        } while (calcTime);
        
    }
}
