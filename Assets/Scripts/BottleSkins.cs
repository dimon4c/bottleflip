﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public partial class Game : MonoBehaviour
{
    public GameObject PurchasePopup;
    public List<GameObject> skinPrices;

    public static  int currentSkin { get { return  PlayerPrefs.GetInt("CurrentSkin", 0); } set { PlayerPrefs.SetInt("CurrentSkin", value); } }

    int totalSkins = 8;
    int purchaseId = 0;
    int[] skinPrice = new int[]{ 0, 20, 300, 500, 700, 800, 900, 1000 };

    bool CheckSkinPurchase(int skinId = 0)
    {
        bool purchased = PlayerPrefs.GetInt("skinPurchased" + skinId, 0) == 1 || skinId == 0;
        return purchased;
    }

    void InitSkins()
    {
        for (int i = 0; i < skinPrices.Count; i++)
        {
            if (CheckSkinPurchase(i))
            {
                skinPrices[i].SetActive(false);
            }
            else
            {
                skinPrices[i].SetActive(true);
            }
        }
        GameProxy.Instance.SetCurrentSkin(currentSkin);
    }

    public void PurchaseSkin()
    {
        if (GameProxy.Instance.SpendCoins(skinPrice[purchaseId]))
        {
            currentSkin = purchaseId;
            PlayerPrefs.SetInt("skinPurchased" + purchaseId, 1);
            HideBottlePopUp();
            skinPrices[currentSkin].SetActive(false);
        }
        else
        {
            Debug.Log("not enough money");
        }
    }

    public  void OnSkinScroll(int currentSkin)
    {
//        if (true)
        HideBottlePopUp();
        if (CheckSkinPurchase(currentSkin))
        {
            if (bottles.Count > currentSkin)
            {
                Game.currentSkin = currentSkin;
                thObject = bottles[currentSkin];
            }
        }
        else
        {
            purchaseId = currentSkin;
//            ShowPopUp();
        }
    }

    public  void OnSkinClick(int currentSkin)
    {
        //        if (true)
        HideBottlePopUp();
        if (!CheckSkinPurchase(currentSkin))
        {
            purchaseId = currentSkin;
            ShowPopUp();
        }
        //*/
        else
        {
            Game.currentSkin = currentSkin;
            GameProxy.Instance.SetCurrentSkin(currentSkin);
        }
        //*/
    }

    void ShowPopUp()
    {
        PurchasePopup.SetActive(true);
    }

    public void HideBottlePopUp()
    {
        PurchasePopup.SetActive(false);
    }

   

}