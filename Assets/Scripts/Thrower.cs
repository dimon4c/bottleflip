﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//using System;

public enum GameType
{
    Throw,
    Jump,
    Flip}

;

public class Thrower : MonoBehaviour
{
    

    public    Rigidbody rb;
    public GameType game;
    public bool throwing = false;
    public Quaternion defRot;
    Vector3 defScale;
    public bool freeze = true;

    void Awake()
    {
//        rb = GetComponent<Rigidbody>();
        defScale = transform.localScale;
        defRot = transform.rotation;
        rb.maxAngularVelocity = 10f;

    }

    public bool freezeX = false;

    public  void SetCollision(CollisionDetectionMode col)
    {
        rb.collisionDetectionMode = col;
    }

    void FixedUpdate()
    {
        if (rb.velocity.y < 0 && rb.velocity.magnitude > 0.5f && transform.position.y < 0.1f)
        {
            throwing = true;
        }
        if (rb.velocity.magnitude < 0.01f && throwing)
        {
//            Debug.Log("Update");
            throwing = false;
//            Invoke("ResetPos", 0.5f);
        }
        if (freeze)
        {
            Vector3 vel;
            vel = rb.angularVelocity;
            vel.y = 0;
            vel.z = 0;
           
            rb.angularVelocity = vel;
        }
        if (freezeX)
        {
            Vector3 vel;
            vel = rb.angularVelocity;

            vel.x = 0;
            rb.angularVelocity = vel;
        }
    }

    public bool IsStanding()
    {
        return rb.velocity.magnitude < 0.01f;
    }

    public bool isFalling()
    {
        return rb.velocity.y < 0;
    }

    public float CurentAngle()
    {
        float angle = Mathf.Abs(Quaternion.Angle(rb.rotation, defRot));
//        Debug.Log("angle " + angle);
        return angle;
    }

    public void ResetPos()
    {
        //*/

        StopScale();
        freeze = true;
        freezeX = false;
        if (rb.isKinematic)
            return;
        rb.isKinematic = true;
        if (defScale == Vector3.zero)
            defScale = transform.localScale;
        else
            transform.localScale = defScale;
        transform.position = Vector3.zero + new Vector3(0.07f, 0.15f);
        transform.rotation = defRot;
        rb.isKinematic = false;

        //*/
    }

    public void ResetPos(Vector3 pos)
    {
        //*/
       
        StopScale();
        freeze = true;
        freezeX = false;
        if (rb.isKinematic)
            return;
        rb.isKinematic = true;
        transform.position = pos;
        transform.rotation = defRot;
        rb.isKinematic = false;
        //*/
    }

    public void Throw(Vector3 throwVector, float multiplier = 1)
    {
        throwVector.x /= 2;
        rb.AddForce(throwVector * multiplier * 0.2f, ForceMode.Impulse);
        rb.AddTorque(new Vector3(-10, 0f, 0f) * 50);
        StartCoroutine(FreezeRot());
    }

    public void Jump(Vector3 throwVector, float multiplier = 1)
    {
        throwVector.x /= 2;

        rb.AddForce(throwVector * multiplier * 0.2f, ForceMode.Impulse);
        rb.AddTorque(new Vector3(-10, 0f, 0f) * 50);
        StartCoroutine(FreezeRot());
//        rb.AddForce(Vector3.up * 20 * 0.3f, ForceMode.Impulse);
//        rb.AddTorque(new Vector3(-7f, 0f, 0f) * 50);
    }

    public void Flip(Vector3 throwVector, float multiplier = 1)
    {
        throwVector.x /= 8;
        rb.AddForce(throwVector * multiplier * 0.3f, ForceMode.Impulse);
        rb.AddTorque(new Vector3(-7f, 0f, 0f) * multiplier);
//        StartCoroutine(FreezeRot());
    }

    IEnumerator FreezeRot()
    {
        yield return new WaitForSeconds(0.1f);
        //        while (!(CurentAngle() >= 175f && CurentAngle() <= 180f))
        while (Quaternion.Angle(rb.rotation, defRot) > 2f)
            yield return new WaitForFixedUpdate();
//        Debug.Log("Frozen");
        freezeX = true;
//        yield return new WaitForFixedUpdate();
//        rb.freezeRotation = false;
//        freeze = false;
    }

    IEnumerator scaler;

    public void StartScale()
    {
        scaler = ThrowScaling();
        StartCoroutine(scaler);
    }

    public void StopScale()
    {
        if (scaler != null)
            StopCoroutine(scaler);
    }

    IEnumerator ThrowScaling()
    {
        for (int i = 0; i < 25; i++)
        {
            transform.localScale -= new Vector3(0.005f, 0.005f, 0.005f);
            yield return new WaitForFixedUpdate();
        }
    }

    public  void OnMouseDownDo()
    {
        freezeX = false;
        freeze = true;
//        rb.constraints = RigidbodyConstraints.FreezeRotationY;
//        rb.constraints = RigidbodyConstraints.FreezeRotationZ;
        switch (game)
        {
            case GameType.Throw:
                break;
            case GameType.Jump:
//                Jump();   
                break;
            case GameType.Flip:
                break;
        }
    }

    void FlipSpotter()
    {
        StartCoroutine(ISpotter());
    }

    bool IsOnTopOrBottom()
    {
        return (Quaternion.Angle(rb.rotation, defRot) < 2f || Quaternion.Angle(rb.rotation, defRot) > 178f);
    }

    IEnumerator ISpotter()
    {
        
        //*/
        float step = 0.13f;
//        Debug.Log("FlipSpotter");
        yield return new WaitUntil(() => rb.angularVelocity.magnitude > 0);
        for (int i = 0; i < 1000; i++)
        {
            yield return new WaitForFixedUpdate();
            Vector3 vel;
            vel = rb.angularVelocity;
//            Debug.Log("vel " + vel);
            /*/
            if (Mathf.Abs(vel.x) > step)
            {
                yield return new WaitForFixedUpdate();
                break;
            }
            //*/
//            if (IsOnTopOrBottom())
            if (Mathf.Abs(vel.x) < 2f && IsOnTopOrBottom())
            {
                Debug.Log("IsOnTopOrBottom");
                vel.x = 0f;
                rb.angularVelocity = vel;
                yield return new WaitForFixedUpdate();
                break;
            }
            //*/
            if (vel.x > 0)
                vel.x -= step;
            else if (vel.x < 0)
                vel.x += step;
            if (Mathf.Abs(vel.x) > step)
                rb.angularVelocity = vel;
            else
                break;
            //*/
            yield return new WaitForFixedUpdate();
        }
        yield return new WaitForFixedUpdate();
        //*/
    }

    public  void OnMouseUpDo(Vector3 start, Vector3 end, float seconds)
    {
       
        if (start.y >= end.y)
        {
            return;
        }
        float dist = Vector3.Distance(end, start);
        Vector3 calcVect = end - start;
       
        float boost = Mathf.Clamp(calcVect.y, 0.5f, 2.5f);
       
        float speedC = boost / seconds;


        Vector3 norm = calcVect.normalized;
        if (game == GameType.Flip)
            norm = new Vector3(norm.x / 8f, norm.y, 0);
        else
            norm = new Vector3(norm.x / 2f, norm.y / 1f, 0);
        float force = 7.5f * speedC;




        #region FlipPart
        /*/
        if (game == GameType.Flip)
        {
            if (dist < 0.08f)
                force /= 2.5f;
            else if (dist < 0.07f)
                force /= 2.6f;
            else if (dist < 0.06f)
                force /= 2.8f;
            else if (dist < 0.05f)
                force /= 3f;
        }
//        Debug.Log("force " + force);
        if (game == GameType.Flip)
        {
            if (force >= 2.5f && force <= 5.8f)
            {
                if (force >= 2.8f && force <= 4f)
                {
                    force = 3.244529f;
                }
                else
                {
                    float dispersion = Mathf.Abs(force - 3.244529f) / 10;
                    float addforce;
                    if (force - 3.244529f < 0)
                        addforce = 3.244529f - dispersion;
                    else
                        addforce = 3.244529f + dispersion;
                
                    force = addforce;
                }
            }
        }
        //*/
        #endregion
//        3.244529
//        force = 3.244529f * 0.7f;
        Vector3 dirForce = norm * force;
        Vector3 dirTorque;
        if (game == GameType.Flip)
        {
//            Debug.Log("dist " + dist);
            float modifier = dist < 0.8f ? dist : 0.8f;
            if (dist < 0.1f)
            {
                modifier = 0.1f;
            }
            dirTorque = new Vector3(-1, 0, 0) * 8 * modifier;
//            Debug.Log("dirTorque " + dirTorque.x);
//            dirTorque = new Vector3(-1, 0, 0) * 25;
        }
        else
        {
            dirTorque = new Vector3(-1, 0, 0) * 25;
        }
       
        if (game == GameType.Jump)
            dirForce *= 1.2f;
        
        dirForce.y = dirForce.y > 5.3f ? 5.3f : dirForce.y;

        //if (dir.magnitude < 200)
//        Debug.Log("Force: " + force + ", norm: " + norm);

       
        /*/
//        calcVect.Normalize();
        multiplier *= 0.01f;
        multiplier += deltaY * 0.1f;
        if (multiplier >= 50)
        {
            multiplier = Random.Range(40, 51);
        }
        if (multiplier <= 20)
        {
            multiplier = 20;
        }
        //*/
        switch (game)
        {
            case GameType.Throw:
//                Throw(calcVect, multiplier);
               


//                Debug.Log("dirForce.y " + dirForce.y);

//                dirForce.z = 30f;
                dirForce.y = 5f;
                rb.AddForce(dirForce, ForceMode.Impulse);
                rb.AddTorque(-dirTorque, ForceMode.Force);
                if (Game.currentSkin != 1)
                    StartScale();
                StartCoroutine(FreezeRot());

                break;
            case GameType.Jump:
//                Jump(calcVect, multiplier);

                dirForce.y = dirForce.y < 3f ? 3f : dirForce.y;
                rb.AddForce(dirForce, ForceMode.Impulse);
                rb.AddTorque(-dirTorque, ForceMode.Force);

                StartCoroutine(FreezeRot());


                break;
            case GameType.Flip:
//                Flip(calcVect, multiplier);
            
                rb.AddForce(dirForce, ForceMode.Impulse);
                rb.AddTorque(dirTorque, ForceMode.Force);
                FlipSpotter();

                break;
        }
    




    }

  

   

}
